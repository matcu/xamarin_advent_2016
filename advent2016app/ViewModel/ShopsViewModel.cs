﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

using advent2016app.Model;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;


namespace advent2016app.ViewModel
{
	public class ShopsViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		void OnPropertyChanged([CallerMemberName] string name = null) =>
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

		public ObservableCollection<Shop> Shops { get; set; }
		public Command GetShopsCommand { get; set; }

		public ShopsViewModel()
		{
			Shops = new ObservableCollection<Shop>();
			GetShopsCommand = new Command(
				async () => await GetShops(),
				() => !IsBusy);
		}

		bool busy;

		public bool IsBusy
		{
			get { return busy; }
			set
			{
				busy = value;
				OnPropertyChanged();
				GetShopsCommand.ChangeCanExecute();
			}
		}

		async Task GetShops()
		{
			if (IsBusy)
				return;

			Exception error = null;
			try
			{
				IsBusy = true;

				using (var client = new HttpClient())
				{
					// サーバーから json を取得します
					var json = await client.GetStringAsync("https://advent2016.azurewebsites.net/api/v1/shop");
					var items = JsonConvert.DeserializeObject<List<Shop>>(json);

					Shops.Clear();
					foreach (var item in items)
						Shops.Add(item);
				}

			}
			catch (Exception ex)
			{
				Debug.WriteLine("Error: " + ex);
				error = ex;
			}
			finally
			{
				IsBusy = false;
			}

			if (error != null)
				await Application.Current.MainPage.DisplayAlert("Error!", error.Message, "OK");
		}
	}

	
}
