﻿using System;
using Xamarin.Forms;
using advent2016app.View;

namespace advent2016app
{
	public class App : Application
	{
		public App()
		{
			MainPage = new advent2016appPage();
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
