﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace advent2016app
{
	public partial class ShopList : ContentPage
	{
		public ShopList()
		{
			InitializeComponent();

			MainPage = new advent2016appPage();
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
