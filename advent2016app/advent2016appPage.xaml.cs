﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

using advent2016app.Model;
using advent2016app.ViewModel;

namespace advent2016app.View
{
	public partial class advent2016appPage : ContentPage
	{
		ShopsViewModel vm;
		public advent2016appPage()
		{
			InitializeComponent();

			//Create the view model and set as binding context
			vm = new ShopsViewModel();
			BindingContext = vm;
		}

}
}
