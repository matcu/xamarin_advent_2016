﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace advent2016app.Model
{
	public class Shop
	{
		public String Id { get; set; }
		public String Name { get; set; }
		public String ZipCode { get; set; }
		public String Address { get; set; }
		public String PhoneNumber { get; set; }
		public String Memo { get; set; }
	}
}
